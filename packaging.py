import os
import subprocess
import random
import string
import json
import time
from cumulusci.tasks.salesforce import BaseSalesforceApiTask
from cumulusci.core.sfdx import sfdx
from cumulusci.core.exceptions import ScratchOrgException

nl = "\n"  # fstrings can't contain backslashes

def parse_output(logger, output, log=False):
    stderr_list = [line.strip() for line in output.stderr_text]
    stdout_list = [line.strip() for line in output.stdout_text]

    if output.returncode:
        logger.error(f"Return code: {output.returncode}")
        for line in stderr_list:
            logger.error(line)
        for line in stdout_list:
            logger.error(line)
        message = f"\nstderr:\n{nl.join(stderr_list)}"
        message += f"\nstdout:\n{nl.join(stdout_list)}"
        raise ScratchOrgException(message)
    elif log:
        for line in stdout_list:
            logger.info(line)

    return stdout_list

class PromoteBetaPackage(BaseSalesforceApiTask):
    name = "PromoteBetaPackage"
    api_version = "46.0"
    task_options = {
        "version": {
            "description": "The id or alias of the package version.", 
            "required": True
        },
        "json": {
            "description": "Format output as JSON."
        }
    }
    def _run_task(self):
        cmd = "force:package:version:promote -p {0}"
        cmd = cmd.format(self.options["version"])
        if "json" in self.options and self.options["json"]:
            cmd += " --json"
        self.logger.info(cmd)
        p = sfdx(cmd)
        parse_output(self.logger, p, True)

class CreateBetaPackage(BaseSalesforceApiTask):
    name = "CreateBetaPackage"
    api_version = "46.0"
    task_options = {
        "name": {
            "description": "The name of the package version.", 
            "required": True
        },
        "description": {
            "description": "A description of the package and what this version contains."
        },
        "devhub":{
            "description": "Dev Hub username to run the package creation against."
        },
        "password": {
            "description": "A password for sharing the package privately with anyone who has the password."
        },
        "post_install_url": {
            "description": "The fully-qualified URL of the post-installation instructions. Instructions are shown as a link after installation and are available from the package detail view."
        },
        "release_notes_url": {
            "description": "The fully-qualified URL of the package release notes. Release notes are shown as a link during the installation process and are available from the package detail view after installation."
        },
        "namespace": {
            "description": "The namespace of the package.  Defaults to project__package__namespace"
        },
        "json": {
            "description": "Format output as JSON."
        }
    }

    def _init_options(self, kwargs):
        super(CreateBetaPackage, self)._init_options(kwargs)

        if "namespace" not in self.options:
            if self.project_config.project__package__namespace is None:
                self.options["namespace"] = self.project_config.project__package__name
            else:
                self.options["namespace"] = self.project_config.project__package__namespace
    
    def _run_task(self):
        cmd = 'force:package:version:create -d force-app/main/default -p {0} -a "{1}" -w 10'
        cmd = cmd.format(self.options["namespace"], self.options["name"])
        if "description" in self.options:
            cmd += " -e " + self.options["description"]
        if "password" in self.options:
            cmd += " -k "+self.options["password"]
        else:
            cmd += " -x"
        if "devhub" in self.options:
            cmd+= " -v "+self.options["devhub"]
        if "json" in self.options and self.options["json"]:
            cmd += " --json"
        self.logger.info(cmd)
        p = sfdx(cmd)
        parse_output(self.logger, p, True)


class InstallPackage(BaseSalesforceApiTask):
    name = "InstallPackage"
    api_version = "46.0"
    task_options = {
        "version": {
            "description": "The id or alias of the package version.", 
            "required": True
        },
        "json": {
            "description": "Format output as JSON."
        }
    }

    def _run_task(self):
        cmd = 'force:package:install -w 10 -p "{0}"'
        cmd = cmd.format(self.options["version"])
        if "json" in self.options and self.options["json"]:
            cmd += " --json"
        self.logger.info(cmd)
        p = sfdx(cmd)
        parse_output(self.logger, p, True)

class DisplayMostRecentPackage(BaseSalesforceApiTask):
    name = "DisplayMostRecentPackage"
    api_version = "46.0"
    def _run_task(self):
        p = sfdx("force:package:version:list --json")

        stdout_list = parse_output(self.logger, p)
        
        dic = json.loads("".join(stdout_list))
        dic = dic["result"]
        max_date = self.to_date(dic[0]["CreatedDate"])
        max_elem = dic[0]
        for elem in dic:
            cur_date = self.to_date(elem["CreatedDate"])
            if cur_date > max_date:
                max_date = cur_date
                max_elem = elem
        with open("packageId.txt", "w") as f:
            f.write(max_elem["SubscriberPackageVersionId"])
            f.close()
        self.logger.info("Newest package id: "+max_elem["SubscriberPackageVersionId"])

    def to_date(self, str_inp):
        return time.strptime(str_inp, "%Y-%m-%d %H:%M")

class InstallDependencies(BaseSalesforceApiTask):
    name = "InstallDependencies"
    api_version = "46.0"
    
    def _run_task(self):
        # The execution of this script stops if a command or pipeline has an error.
        self.username= self.org_config.username
        with open('sfdx-project.json') as f:
            sfdx_text = json.load(f)
            installed = []
            for package in sfdx_text["packageDirectories"]:
                if "dependencies" not in package:
                    continue
                dependencies = package["dependencies"]
                dependencies.reverse()
                for dependency in dependencies:
                    if sfdx_text["packageAliases"][dependency["package"]] in installed:
                        continue
                    self.install_package(sfdx_text["packageAliases"][dependency["package"]], installed)

        
    def install_package(self, package, installed):
        if package in installed or self.is_installed(package):
            return

        self.logger.info("Retrieving dependencies for package Id: "+package)
        
        p = sfdx("force:data:soql:query -u {0} -t -q \"SELECT Dependencies FROM SubscriberPackageVersion WHERE Id=\'{1}\'\" --json".format(self.username, package))

        stdout_list = parse_output(self.logger, p)

        dependencies = json.loads("".join(stdout_list))["result"]["records"][0]["Dependencies"]

        # If the parsed dependencies is None, the package has no dependencies. Otherwise, parse the result into a list of ids.

        if dependencies != "None" and dependencies is not None:

            # Then loop through the ids to install each of the dependent packages.

            ids = dependencies["ids"]

            dependencies = []
            i=0
            for id in ids:
                id = id["subscriberPackageVersionId"]
                dependencies.append(id)
                self.write_new_entry(id, i)
                i+=1

            self.logger.info("The package you are installing depends on these packages (in correct dependency order): "+ ", ".join(dependencies))

            for id in dependencies:
                p = sfdx(f"force:package:install --package {id} -u {self.username} -w 10 --publishwait 10")
                installed.append(id)
                stdout_list = parse_output(self.logger, p, True)
            
        p = sfdx(f"force:package:install --package {package} -u {self.username} -w 10 --publishwait 10")
        installed.append(package)
        stdout_list = parse_output(self.logger, p, True)

    def write_new_entry(self, package_id, i):
        p = sfdx("force:data:soql:query -u {0} -t -q \"SELECT SubscriberPackageId FROM SubscriberPackageVersion WHERE Id=\'{1}\'\" --json".format(self.username, package_id))

        stdout_list = parse_output(self.logger, p)

        id = json.loads("".join(stdout_list))["result"]["records"][0]["SubscriberPackageId"]

        p = sfdx("force:data:soql:query -u {0} -t -q \"SELECT NamespacePrefix,Name FROM SubscriberPackage WHERE Id=\'{1}\'\" --json".format(self.username, id))

        stdout_list = parse_output(self.logger, p)
        
        name = json.loads("".join(stdout_list))["result"]["records"][0]["Name"]
        with open('sfdx-project.json','r+') as f:
            sfdx_text = json.load(f)
            if name not in sfdx_text["packageAliases"].keys():
                sfdx_text["packageAliases"][name] = package_id
                sfdx_text["packageDirectories"][0]["dependencies"].insert(i,{"package":name})
                f.seek(0)
                json.dump(sfdx_text,f, indent=4)
                f.truncate()

    def is_installed(self, id):
        p = sfdx(f"force:data:soql:query -u {self.username} -t -q \"SELECT SubscriberPackageVersionId FROM InstalledSubscriberPackage\" --json")

        stdout_list = parse_output(self.logger, p)

        for record in json.loads("".join(stdout_list))["result"]["records"]:
            if record["SubscriberPackageVersionId"] == id:
                return True

        return False